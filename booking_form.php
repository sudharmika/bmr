<?php
error_reporting(0);

// Change this to receiver email address
$to="balimountainretreat@gmail.com";

// Fetching Values from URL.
$name = $_POST['name1'];
$email = $_POST['email1'];
$phone = $_POST['phone1'];
$lname = $_POST['lname1'];

$rooms = $_POST['rooms1'];
$roomtype = $_POST['roomtype1'];
$adults = $_POST['adults1'];
$children = $_POST['children1'];

$arrival = $_POST['arrival1'];
$departure = $_POST['departure1'];

$message = $_POST['message1'];

$email = filter_var($email, FILTER_SANITIZE_EMAIL); // Sanitizing E-mail.
// After sanitization Validation is performed
if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
$subject = "Room Reservation Request";
// To send HTML mail, the Content-type header must be set.
$headers = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= 'From:' . $email. "\r\n"; // Sender's Email
$headers .= 'Cc:' . $email. "\r\n"; // Carbon copy to Sender
$template = '<div style="padding:50px; color:white;">Hello ' . $name . ',<br/>'
. '<br/>Thank you for contacting Bali Mountain Retreat!<br/><br/>'
. 'Name:' . $name ." ".$lname. '<br/>'
. 'Email:' . $email . '<br/>'
. 'Phone No:' . $phone . '<br/>'
. 'Room Type:' . $roomtype . '<br/>'
. 'No. of Rooms:' . $rooms . '<br/>'
. 'No. of Adults:' . $adults . '<br/>'
. 'No. of Children:' . $children . '<br/>'
. 'Arrival Date:' . $arrival . '<br/>'
. 'Departure Date:' . $departure . '<br/>'
. 'Additional Info:' . $message . '<br/><br/>'
. 'Your reservation request has been received. We will contact you to confirm as soon as possible.</div>';
$sendmessage = "<div style=\"background-color:#7E7E7E; color:white;\">" . $template . "</div>";
// Message lines should not exceed 70 characters (PHP rule), so wrap it.
$sendmessage = wordwrap($sendmessage, 70);
// Send mail by PHP Mail Function.
mail($to, $subject, $sendmessage, $headers);
echo "Your message has been received, we will contact you as soon as possible.";
} else {
echo "<span>* invalid email *</span>";
}
?>

