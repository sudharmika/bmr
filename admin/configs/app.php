<?php 
define('ROOT_DIR', dirname(dirname(__FILE__)));
define('CONFIG_DIR', ROOT_DIR . '/configs' );
define('LOG_DIR', ROOT_DIR . '/logs');
define('LOG_ERROR_DIR', LOG_DIR . '/errors');
define('MODEL_DIR', ROOT_DIR . '/models');
define('VIEW_DIR', ROOT_DIR . '/views');
define('CORE_DIR', ROOT_DIR . '/core');
define('ERROR_DIR', CORE_DIR . '/error');
define('DATABASE_DIR', CORE_DIR . '/database');
define('DEBUG', true);

