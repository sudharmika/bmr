<?php 
function h($string) {
    $string = nl2br(filter_var($string, FILTER_SANITIZE_STRING));

    return $string;
}

function format_date($date, $format = 'Y-m-d H:i:s') {
    $date         = date_create($date);
    $dateFormated = date_format($date, $format);

    return $dateFormated;
}
