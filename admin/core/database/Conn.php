<?php 
include_once CONFIG_DIR . '/database.php';

Class Conn
{
    private $connection;
    private $errorHandler;

    public function __construct()
    {
        $this->errorHandler = new ErrorHandler();
    }

    public function connect()
    {
        try {         
            $this->connection = new mysqli(HOST, USERNAME, PASSWORD, DATABASE_NAME);

            if ($this->connection->connect_error) {
                throw new Exception($this->connection->connect_error); 
            } else {
                return $this->connection;
            }
        } catch (Exception $e) {
            $this->errorHandler->customError($e); 
        }
    }
}
