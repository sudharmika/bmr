<?php
include_once DATABASE_DIR . '/Conn.php';

class Query 
{
    protected $table;
    protected $orderBy;
    protected $columns;
    protected $conn;
    protected $errorHandler;

    public function __construct($tableName) 
    {
        $this->conn         = (new Conn)->connect();
        $this->errorHandler = new ErrorHandler();
        $this->table        = $tableName;
    }

    public function select($columns = '*')
    {
        $this->columns = $columns;

        return $this;
    }

    public function orderBy($fieldName, $order = 'ASC')
    {
        $fieldNama = trim($fieldName);
        $order     = trim(strtoupper($order));
        
        if ($this->orderBy == '') {
            $this->orderBy = " ORDER BY {$fieldName} {$order} ";
        } else {
            $this->orderBy .= ", {$fieldName} {$order} ";
        }

        return $this;
    }

    public function insert($data = [])
    {
        $keys   = implode(', ', array_keys($data));
        $values = implode("', '", $this->safeData(array_values($data)));

        $query = "INSERT INTO {$this->table} "
               . "({$keys}) "
               . "VALUES ('{$values}') ";

        try {
            $result = $this->conn->query($query);

            if (!$result) {
                throw new Exception($this->conn->error);
            }
        } catch (Exception $e) {
            $this->errorHandler->customError($e);
        }
    }

    public function get()
    {
        $query = "SELECT {$this->columns} "
               . "FROM {$this->table} {$this->orderBy} ";
        
        try {         
            $result = $this->conn->query($query);

            if (!$result) {
                throw new Exception($this->conn->error); 
            } 

            $rows = array();

            while ($row = $result->fetch_assoc()) {
                $rows[] = $row;
            }

            return $rows;       
        } catch (Exception $e) {
            $this->errorHandler->customError($e);
        }
    }

    public function safeData($data) 
    {
        $result = array();

        if (is_array($data)) {
            foreach ($data as $row) {
                $result[] = $this->conn->real_escape_string($row);
            }
        }

        return $result;
    }
}
