<?php
class ErrorHandler
{
    private $logFile;

    public function __construct()
    {
        $this->logFile = LOG_ERROR_DIR . '/' . date('Ym') . '_errors.log';

        error_reporting(E_ALL);
        
        set_error_handler(array($this, 'errorHandler'));
        set_exception_handler(array($this, 'customError'));
        register_shutdown_function(array($this, 'errorShutdownHandler'));
    }

    public function customError($msg) 
    {
        $message =  date('Y - m - d') . ' - ';
        $message .= "{$msg} \r\r"; 

        $this->messageHandler($message);
    }

    public function errorHandler($errno, $errstr, $errfile, $errline) 
    {
        $message =  date('Y - m - d') . ' - ';
        $message .= "ERROR [{$errno}] on line - {$errline} \r "; 
        $message .= "{$errstr} \r ";
        $message .= "in file {$errfile} \r\r";

        $this->messageHandler($message);
    }

    public function errorShutdownHandler()
    {
        $lastError = error_get_last();

        if ($this->isFatal($lastError['type'])) {
            $this->errorHandler(E_ERROR, $lastError['message'], $lastError['file'], $lastError['line']);
        }
    }

    private function isFatal($type)
    {
        return in_array($type, array(E_ERROR, E_CORE_ERROR, E_COMPILE_ERROR, E_PARSE));
    }
 
    public function errorCode($code, $view = true)
    {
        http_response_code($code);

        if ($view) {
            include_once VIEW_DIR . "/errors/{$code}.php";
        }
    }

    private function messageHandler($message) 
    {
        if (!DEBUG) {
            ini_set('display_errors', 'Off');

            error_log($message, 3, $this->logFile);
            
            $this->errorCode(500);
        } else { 
            $this->errorCode(500, false);

            die('Error : ' . $message);
        }
    }
}
