<?php 
class Input
{
    public $all;

    public function __construct()
    {
        $this->all = $_REQUEST;
    }

    public function get($field)
    {
        return ($this->all[$field]) ?? null; 
    }

    public function isMethod($method)
    {
        return strtolower($_SERVER['REQUEST_METHOD']) == strtolower($method);
    }
}
