<?php
include_once DATABASE_DIR . '/Query.php';
include_once CORE_DIR . '/function.php';

class Model
{
    private $query;
    protected $tableName;
    protected $filledColumn = array();
    protected $createdDate  = true;

    public function __construct()
    {
        $this->query = new Query($this->tableName);
    }

    public function get()
    {
        if ($this->createdDate) {
            $this->query->orderBy('created_date', 'DESC');
        }

        $column = '*';

        if ($this->filledColumn) {
            $column = implode(',', $this->filledColumn);
        }

        $data = $this->query
                     ->select($column)
                     ->get();

        return $data;
    }

    public function insert($values) 
    {
        $data = $this->getInsertData($values);

        if ($this->createdDate) {
            $data['created_date'] = format_date('now');
        }

        $this->query->insert($data);
    }

    private function getInsertData($insertData)
    {
        if (!empty($this->filledColumn)) {
            foreach ($this->filledColumn as $column) {
                if (isset($insertData[$column])) {
                    $data[$column] = $insertData[$column];
                }
            }

            return $data;
        } 

        return $insertDalues;
    }
}
