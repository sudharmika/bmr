<?php 
class Validation 
{
    public  $errors;
    private $name;
    private $value;
    public  $bail = true;
    
    public function validate($data, $validationRule)
    {
        foreach ($validationRule as $fieldName => $rules) {
            $rules = explode('|', $rules);
            $value = ($data[$fieldName]) ?? null;

            $this->name  = $fieldName;
            $this->value = $value;

            $this->rules($rules);
        }
    }

    private function rules($rules)
    {   
        $param = NULL; 

        foreach ($rules as $rule) {
            $rule   = explode(':', $rule);
            $method = $rule[0];
                    
            if (count($rule) > 1) {
                $param  = $rule[1];
            }

            if ($method) {
                if ($error = call_user_func(array($this, $method), $param)) {
                    $this->errors[] = $error;
                }
                
                if ($this->bail && $error) {
                    break;
                }
            }
        }
    }

    private function required()
    {
        if ($this->isEmpty()) {
            return $this->name . ' ' . 'Must be filled in';
        }
    }

    private function lengthBetween($value)
    {
        $length = explode(',', $value);

        if ($this->valueLength() < $length[0] || $this->valueLength() > $length[1]) {
            return "Your {$this->name} must be {$length[0]} to {$length[1]} Character long";
        }
    }

    private function isEmpty()
    {
        return $this->valueLength() == 0;
    }

    private function valueLength() 
    {
        return strlen(trim($this->value));
    }
}
