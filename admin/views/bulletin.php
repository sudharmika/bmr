<!DOCTYPE html>
<html>
    <head>
        <title>Challenge 30 - Level 2</title>
        <style>
            .half{
                float:left;
                width:50%;
            }
        </style>
    </head>

    <body>
        <section style="max-width:900px; margin:0 auto; border: 2px solid #000; padding:30px;">
        
            <div>
                <?php if (isset($errors)) : ?>
                    <?php foreach($errors as $error) : ?>
                        <?php echo $error ?>
                        <br>
                    <?php endforeach ?>
                <?php endif ?>
            </div>

            <form method="post" action="" style="margin-bottom:50px;">
                <p>Title</p>
                <input name="title" style="width:100%" value="<?= $input->get('title') ?>">
                <p>Body</p>
                <textarea name="body" style="width:100%; height:150px;"><?= $input->get('body') ?></textarea>
                
                <input name="submit" type="submit" value="submit">
            </form>

            <?php if (!$bulletins) : ?>
                <p>There's No Data Available</p>
            <?php endif ?>
            
            <?php foreach ($bulletins as $bulletin) : ?>
                <div>
                    <?php echo h($bulletin['title']) ?>
                </div>
                <div class="half">
                    <?php echo h($bulletin['body']) ?>
                </div>
                <div class="half">
                    <?php echo format_date($bulletin['created_date'], 'd-m-Y H:i') ?>
                </div>
                <hr style="clear:both">
            <?php endforeach ?>
        </section>

    </body>

</html>
