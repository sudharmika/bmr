<?php
include_once CORE_DIR . '/model.php';

class Bulletin extends Model 
{
   protected $tableName    = 'bulletin';
   protected $filledColumn = ['title', 'body', 'created_date'];
}
