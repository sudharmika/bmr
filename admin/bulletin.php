<?php
include_once MODEL_DIR . '/Bulletin.php';
include_once CORE_DIR . '/Validation.php';
include_once CORE_DIR . '/Input.php';
include_once CORE_DIR . '/function.php';

$bulletin = new Bulletin();
$input    = new Input();

if ($input->get('submit') && $input->isMethod('POST')) {
    $validation = new Validation();

    $validation->validate($input->all, [
        'title' => 'required|lengthBetween:10,32',
        'body'  => 'required|lengthBetween:10,200'
    ]);

    if (!$validation->errors) {
        $bulletin->insert($input->all);

        header('location: ' . $_SERVER['REQUEST_URI']);
        exit;
    }

    $errors = $validation->errors;
}

$bulletins = $bulletin->get();

include_once VIEW_DIR . '/bulletin.php';
