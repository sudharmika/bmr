$(document).ready(function()
	{

		$('.flexslider').flexslider(
			{
				animation: "fade",
				controlNav: true,
				smoothHeight: true,
				before: function()
				{
					onBefore();
				},
				after: function()
				{
					onAfter();
				}
			});



		$('.caption2').css("display", "block").animate({ left: "20px" }).fadeIn(500);
		$('.book-now2').delay(300).show(0).animate({ left: "-20px" }).fadeIn(2000);

		function onBefore()
		{
			$('.caption2').css(
				{
					'display' : 'none',
					left : '0px'
				});
			$('.book-now2').css(
				{
					'display' : 'none',
					left : '0px'
				});
		}
		function onAfter()
		{
			$('.caption2').css("display", "block").animate({ left: "20px" }).fadeIn(500);
			$('.book-now2').delay(300).show(0).animate({ left: "-20px" }).fadeIn(2000);
		}


		$('.counter').counterUp(
			{
				delay: 100, // the delay time in ms
				time: 1000 // the speed time in ms
			});

		// portfolio hover
		$('.portfolioHover span').css({opacity:0});
		$(".portfolio").on(
			{
				mouseenter: function ()
				{
					$(this)
					.find('.portfolioHover img').stop()
					.animate({ opacity:0.3 }, 0, function() {})
					.end()
					.find('.portfolioHover span').stop()
					.animate({ opacity:1 }, 0, function() {})
					.end()
					.find('.plus-btn').stop()
					.css('background', '#FF6239')
					.end()
					.find('.plus-btn-blue').stop()
					.css('background', '#3d9ae8')
					.end()
					.find('.plus-btn-red').stop()
					.css('background', '#f12537')
					.end()
					.find('.plus-btn-green').stop()
					.css('background', '#53ad5e')
					.end()
					.find('.acc-box .box h3').stop()
					.css('color', '#FF6239')
					.end()
					.find('.acc-triangle-bottom').stop()
					.css("border-bottom-color", "#FC6339")
					.end()
					.find('.acc-triangle-blue').stop()
					.css("border-bottom-color", "#3d9ae8")
					.end()
					.find('.acc-triangle-red').stop()
					.css("border-bottom-color", "#f12537")
					.end()
					.find('.acc-triangle-green').stop()
					.css("border-bottom-color", "#53ad5e")
					.end()
					.find('.portfolioHover span img').stop()
					.animate({ opacity:1 }, 0, function() {})
					.end()
				},
				mouseleave: function ()
				{
					$(this)
					.find('.portfolioHover img').stop()
					.animate({ opacity:1 }, 0, function() {})
					.end()
					.find('.portfolioHover span').stop()
					.animate({ opacity:0 }, 0, function() {})
					.end()
					.find('.plus-btn').stop()
					.css('background', '#FFFFFF')
					.end()
					.find('.acc-triangle-blue').stop()
					.css("border-bottom-color", "#dbdbdb")
					.end()
					.find('.acc-triangle-red').stop()
					.css("border-bottom-color", "#dbdbdb")
					.end()
					.find('.acc-triangle-green').stop()
					.css("border-bottom-color", "#dbdbdb")
					.end()
					.find('.acc-triangle-bottom').stop()
					.css('border-bottom-color', '#dbdbdb')
					.end()
				}
			});

		$(".stats-box .box").on(
			{
				mouseenter: function ()
				{
					$(this)
					.find('.stats-triangle-bottom').stop()
					.css('border-bottom-color', '#392934')
					.end()
					.find('.stats-triangle-default').stop()
					.css("border-bottom-color", "#392934")
					.end()
					.find('.stats-triangle-blue').stop()
					.css('border-bottom-color', '#392934')
					.end()
					.find('.stats-triangle-red').stop()
					.css('border-bottom-color', '#392934')
					.end()
					.find('.stats-triangle-green').stop()
					.css('border-bottom-color', '#392934')
					.end()
				},
				mouseleave: function ()
				{
					$(this)
					.find('.stats-triangle-default').stop()
					.css("border-bottom-color", "#FF6239")
					.end()
					.find('.stats-triangle-bottom').stop()
					.css('border-bottom-color', '#FC6339')
					.end()
					.find('.stats-triangle-blue').stop()
					.css('border-bottom-color', '#3d9ae8')
					.end()
					.find('.stats-triangle-red').stop()
					.css('border-bottom-color', '#f12537')
					.end()
					.find('.stats-triangle-green').stop()
					.css('border-bottom-color', '#53ad5e')
					.end()
				}
			});



		$(".navbar-default .navbar-nav > li.dropdown").on(
			{
				mouseenter: function ()
				{
					$('ul.dropdown-menu', this).stop(true, true).slideDown('fast');
					$(this).addClass('open');
				},
				mouseleave: function ()
				{
					$('ul.dropdown-menu', this).stop(true, true).slideUp('fast');
					$(this).removeClass('open');
				}
			});




		$window = $(window);
		var scrollbg=$('section[data-type="background"]');
		for(var i = 0; i < scrollbg.length; i++)
		{
			// declare the variable to affect the defined data-type
			var $scroll = $(this);

			$(window).scroll(function()
				{
					// HTML5 proves useful for helping with creating JS functions!
					// also, negative value because we're scrolling upwards
					var yPos = -($window.scrollTop() / $scroll.data('speed'));

					// background position
					var coords = '100% '+ yPos + 'px';

					// move the background
					$scroll.css({ backgroundPosition: coords });
				}); // end window scroll
		}  // end section function



		$('#imageGallery').lightSlider(
			{
				adaptiveHeight:true,
				gallery:true,
				item:1,
				slideMargin:0,
				currentPagerPosition:'left',
				thumbItem:9
			});

		var $cs = $('.styled').customSelect();
		$('.datepicker').datepicker();


		$("a[data-gal^='prettyPhoto']").prettyPhoto(
			{
				theme: 'light_square', /* light_rounded / dark_rounded / light_square / dark_square / facebook */
			});


		/* initialize shuffle plugin */
		var $grid = $('#grid');

		$grid.shuffle(
			{
				itemSelector: '.item' // the selector for the items in the grid
			});

		/* reshuffle when user clicks a filter item */
		$(document).on('click', '#filters a', function(e)
			{
				e.preventDefault();

				// set active class
				$('#filters a').removeClass('current');
				$(this).addClass('current');

				// get group name from clicked item
				var groupName = $(this).attr('data-group');

				// reshuffle grid
				$grid.shuffle('shuffle', groupName );
			});
	});